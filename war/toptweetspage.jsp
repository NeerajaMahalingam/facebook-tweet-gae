<%@page import="com.google.appengine.api.datastore.Query.SortDirection"%>
<%@page import="com.google.appengine.api.datastore.Query"%>
<%@page import="com.google.appengine.api.datastore.Query.FilterOperator"%>
<%@page import="com.google.appengine.api.datastore.Key"%>
<%@page import="com.google.appengine.api.datastore.KeyFactory"%>
<%@page
	import="com.google.appengine.api.datastore.DatastoreServiceFactory"%>
<%@page import="com.google.appengine.api.datastore.DatastoreService"%>
<%@page import="com.google.appengine.api.datastore.PreparedQuery"%>
<%@page import="com.google.appengine.api.datastore.Entity"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<title>Top tweets page</title>
<link rel="stylesheet" href="pagedesign.css">
<script src="jquery-3.2.1.js"></script>
<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId : '189117331635390',
			xfbml : true,
			version : 'v2.1'
		});

		function onLogin(response) {
			if (response.status == 'connected') {
				FB.api('/me?fields', function(data) {
					var welcomeBlock = document.getElementById('fb-welcome');
					welcomeBlock.innerHTML = 'Welcome, ' + data.name + '!';
					console.log(data);

					var userName = document.getElementById('tweet_user_name');
					userName.value = data.name;

					var userId = document.getElementById('tweet_user_id');
					userId.value = data.id;
				});
			} else {
				var welcomeBlock = document.getElementById('fb-welcome');
				welcomeBlock.innerHTML = 'Cant get data ' + response.status
						+ '!';
			}
		}

		FB.getLoginStatus(function(response) {
			// Check login status on load, and if the user is
			// already logged in, go directly to the welcome message.
			if (response.status == 'connected') {
				onLogin(response);
			} else {
				// Otherwise, show Login dialog first.
				FB.login(function(response) {
					onLogin(response);
				}, {
					scope : 'user_friends, email'
				});
			}
		});
	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	function callTweetPage() {
		var jspcall = "tweetpage.jsp?userid="
				+ document.getElementById('tweet_user_id').value;
		window.location.href = jspcall;
	}
</script>
</head>
<body>
	<h1>
		<div style="text-align: center">My Twitter Application</div>
	</h1>
	<div class="topnav" id="myTopnav">
		<a href="javascript:callTweetPage();">Tweet</a> <a
			href="friendspage.jsp">Friends</a> <a href="toptweetspage.jsp">Top
			Tweets</a>
	</div>
	<h2 id="fb-welcome"></h2>

	<%
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		Query q = new Query("Tweet").addSort("visitedcnt", SortDirection.DESCENDING);
		int count = 0;
		PreparedQuery pq = ds.prepare(q);
	%>
	<table id="tweet">
		<tr>
			<th align="left">UserName</th>
			<th align="left">Message</th>
			<th align="left">Created at</th>
			<th align="left">Vistied Count</th>
		</tr>

		<%
			for (Entity u1 : pq.asIterable()) {
				if (count < 10) {
					count++;
					String userid = u1.getProperty("user_id").toString();
					String username = u1.getProperty("user_name").toString();
					String status = u1.getProperty("status").toString();
					String timestamp = u1.getProperty("timestamp").toString();
					int visitedCnt = Integer.parseInt(u1.getProperty("visitedcnt").toString());
					long tweetid = u1.getKey().getId();
		%>
		<tr>
			<form>
				<td><%=username%></td>
				<td><%=status%></td>
				<td><%=timestamp%></td>
				<td><%=visitedCnt%></td>
			</form>
		</tr>

		<%
			}
		%>
		<%
			}
		%>
	</table>
	<input type="hidden" name="user_name" id="tweet_user_name">
	<input type="hidden" name="user_id" id="tweet_user_id">
</body>
</html>