<%@page
	import="com.google.appengine.api.datastore.EntityNotFoundException"%>
<%@page import="com.google.appengine.api.datastore.Key"%>
<%@page import="com.google.appengine.api.datastore.Entity"%>
<%@page import="com.google.appengine.api.datastore.PreparedQuery"%>
<%@page import="com.google.appengine.api.datastore.Query.FilterOperator"%>

<%@page import="com.google.appengine.api.datastore.KeyFactory"%>
<%@page import="com.google.appengine.api.datastore.Query"%>
<%@page
	import="com.google.appengine.api.datastore.DatastoreServiceFactory"%>
<%@page import="com.google.appengine.api.datastore.DatastoreService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="pagedesign.css">
<title>Tweet Display page</title>
<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId : '189117331635390',
			xfbml : true,
			version : 'v2.1'
		});

		function onLogin(response) {
			if (response.status == 'connected') {
				FB.api('/me?fields', function(data) {
					var welcomeBlock = document.getElementById('fb-welcome');
					welcomeBlock.innerHTML = 'Welcome, ' + data.name + '!';

					var userName = document.getElementById('tweet_user_name');
					userName.value = data.name;

					var userId = document.getElementById('tweet_user_id');
					userId.value = data.id;

					FB.api("/" + data.id + "/picture?redirect=0", function(
							response) {
						if (response && !response.error) {
							var userDP = document
									.getElementById('tweet_user_dp');
							userDP.value = response.data.url;
							console.log(response.data.url);
							/* handle the result */
						}
					}

					);

					var userDP = document.getElementById('tweet_user_dp');
					userName.value = response.type;

					console.log(data);
				});
			} else {
				var welcomeBlock = document.getElementById('fb-welcome');

				welcomeBlock.innerHTML = 'Cant get data ' + response.status
						+ '!';
			}
		}

		FB.getLoginStatus(function(response) {
			// Check login status on load, and if the user is
			// already logged in, go directly to the welcome message.
			if (response.status == 'connected') {
				onLogin(response);
			} else {
				// Otherwise, show Login dialog first.
				FB.login(function(response) {
					onLogin(response);
				}, {
					scope : 'user_friends, email'
				});
			}
		});
	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
</head>
<body>

	<%
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		//Query q = new Query("Tweet").addFilter("ID/Name", FilterOperator.EQUAL, request.getParameter("tweetid"));

		String tweetidFromurl = request.getParameter("tweetid").trim();

		Key key = KeyFactory.createKey("Tweet", Long.parseLong(tweetidFromurl));
		System.out.println("Key value is:" + key);

		try {
			Entity u1 = ds.get(key);
			System.out.println("The entity value of e3:" + u1);
			String userid = u1.getProperty("user_id").toString();
			String username = u1.getProperty("user_name").toString();
			String status = u1.getProperty("status").toString();
			String timestamp = u1.getProperty("timestamp").toString();
			int visitedCnt = Integer.parseInt(u1.getProperty("visitedcnt").toString());
			String userdp = u1.getProperty("picture").toString();
			visitedCnt++;
			u1.setProperty("visitedcnt", visitedCnt);

			ds.put(u1);
			long tweetid = u1.getKey().getId();
	%>
	<h1>
		<div style="text-align: center">My Twitter Application</div>
	</h1>
	<div class="topnav" id="myTopnav">
		<a href="tweetpage.jsp?userid=<%=userid%>">Tweet</a> <a
			href="friendspage.jsp?userid=<%=userid%>">Friends</a> <a
			href="toptweetspage.jsp">Top Tweets</a>
	</div>
	<h2 id="fb-welcome"></h2>
	<input type="hidden" name="user_dp" id="tweet_user_dp">

	<table id="tweet">
		<tr>
			<th>Display Picture</th>
			<th>Name</th>
			<th>Message</th>
			<th>Created at</th>
		</tr>
		<tr>
			<td><img src="<%=userdp%>"></td>
			<td><%=username%></td>
			<td><%=status%></td>
			<td><%=timestamp%></td>
		</tr>
	</table>
	<%
		} catch (EntityNotFoundException e3) {
			e3.printStackTrace();
		}
	%>
</body>
</html>




