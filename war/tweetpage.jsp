<%@page import="com.google.appengine.api.datastore.Query.SortDirection"%>
<%@page import="com.google.appengine.api.datastore.Query"%>
<%@page import="com.google.appengine.api.datastore.Query.FilterOperator"%>
<%@page import="com.google.appengine.api.datastore.Key"%>
<%@page import="com.google.appengine.api.datastore.KeyFactory"%>
<%@page
	import="com.google.appengine.api.datastore.DatastoreServiceFactory"%>
<%@page import="com.google.appengine.api.datastore.DatastoreService"%>
<%@page import="com.google.appengine.api.datastore.PreparedQuery"%>
<%@page import="com.google.appengine.api.datastore.Entity"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="pagedesign.css">
<title>Tweet page</title>
<script src="jquery-3.2.1.js"></script>
<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId : '189117331635390',
			xfbml : true,
			version : 'v2.1'
		});

		function onLogin(response) {
			if (response.status == 'connected') {
				FB.api('/me', function(data) {

					var welcomeBlock = document.getElementById('fb-welcome');
					welcomeBlock.innerHTML = 'Welcome, ' + data.name + '!';

					var userName = document.getElementById('tweet_user_name');
					userName.value = data.name;

					var userId = document.getElementById('tweet_user_id');
					userId.value = data.id;

					FB.api("/" + data.id + "/picture?redirect=0", function(
							response) {
						if (response && !response.error) {
							var userDP = document
									.getElementById('tweet_user_dp');
							userDP.value = response.data.url;
							console.log(response.data.url);
							/* handle the result */
						}
					}

					);
				});
			} else {
				var welcomeBlock = document.getElementById('fb-welcome');

				welcomeBlock.innerHTML = 'Cant get data ' + response.status
						+ '!';
			}
		}

		FB.getLoginStatus(function(response) {
			// Check login status on load, and if the user is
			// already logged in, go directly to the welcome message.
			if (response.status == 'connected') {
				onLogin(response);
			} else {
				// Otherwise, show Login dialog first.
				FB.login(function(response) {
					onLogin(response);
				}, {
					scope : 'user_friends, email'
				});
			}
		});
	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	function ShareDialog() {
		console.log("Inside ShareDialog");
		var hLink = document.getElementById('hLink');
		console.log(hLink);
		var finalLink = "https://apps.facebook.com/189117331635390/displaypage.jsp?tweetid="
				+ hLink.value;
		console.log(finalLink);
		FB.ui({
			method : 'share',
			href : finalLink,
		}, function(response) {
		});
	}

	function SendDirect() {
		console.log("Inside ShareDialog");
		var hLink = document.getElementById('hLink');
		console.log(hLink);
		var finalLink = "https://apps.facebook.com/189117331635390/displaypage.jsp?tweetid="
				+ hLink.value;
		FB.ui({
			method : 'send',
			link : finalLink,
		});
	}
</script>
</head>
<body>
	<h1>
		<div style="text-align: center">My Twitter Application</div>
	</h1>
	<div class="topnav" id="myTopnav">
		<a href="tweetpage.jsp">Tweet</a> <a href="friendspage.jsp">Friends</a>
		<a href="toptweetspage.jsp">Top Tweets</a>
	</div>
	<h2 id="fb-welcome"></h2>

	<h4>
		<label align="center" id="postTweet" value="Post a tweet">Post
			a Tweet!!!</label>
	</h4>
	<form>
		<table id="texttable" align="justify">
			<form>
				<td>
					<%
						if (request.getAttribute("tweetid") != null) {
							String hLink = request.getAttribute("tweetid").toString();
							System.out.println("hlink inside if statement" + hLink);
					%> <input
					type="hidden" name="hLinkLabel" id="hLink" value="<%=hLink%>">
					</input> <%
 	}
 %>
				</td>
				<tr>
					<td class="div1" rowspan="3" width=50%><textarea
							name="content" id="TweetTextArea" ROWS=10 COLS=40></textarea></td>
					<td>
						<table>
							<tr>

								<td><input class="btn1" type="button" value="Submit"
									id="TweetSubmitButton"
									onclick="form.action='FirstServletpage';form.submit();" /></td>
							</tr>
							<tr>
								<td><input class="btn1" type="button" value="Share Tweet"
									id="ShareTweetButton" onclick="ShareDialog();"></td>
								</td>

							</tr>

							<tr>
								<td><input class="btn1" type="button"
									value="Send Direct Message" id="Senddirectmsg"
									onclick="SendDirect();"></td>
							</tr>
						</table>
					</td>

				</tr>

			</form>
		</table>

		<br> <br> <input type="hidden" name="user_name"
			id="tweet_user_name"> <input type="hidden" name="user_id"
			id="tweet_user_id"> <input type="hidden" name="user_dp"
			id="tweet_user_dp">
		</div>
	</form>
	<%
		String loggedUserId = "";
		if (request.getAttribute("userid") != null) {
			loggedUserId = request.getAttribute("userid").toString();
		}

		if (request.getParameter("userid") != null) {
			loggedUserId = request.getParameter("userid").toString();
		}

		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query("Tweet").addFilter("user_id", FilterOperator.EQUAL, loggedUserId);
		PreparedQuery pq = ds.prepare(q);
	%>

	<h4>
		<label id="mytweets" value="My Tweets!!!">My Tweets!!!</label>
	</h4>
	<table id="tweet" align="justify">
		<tr>
			<th align="left">Message</th>
			<th align="left">Created at</th>
			<th align="left">Vistied Count</th>
			<th align="left">Delete</th>
		</tr>

		<%
			for (Entity u1 : pq.asIterable()) {
				String username = u1.getProperty("user_name").toString();
				String userid = u1.getProperty("user_id").toString();
				String status = u1.getProperty("status").toString();
				String timestamp = u1.getProperty("timestamp").toString();
				int visitedCnt = Integer.parseInt(u1.getProperty("visitedcnt").toString());
				long tweetid = u1.getKey().getId();
				String userdp = u1.getProperty("picture").toString();
		%>
		<tr>
			<form>
				<td><%=status%></td>
				<td><%=timestamp%></td>
				<td><%=visitedCnt%></td>
				<td><input type="button" value="Delete" id="DeleteButton"
					width=50% onclick="form.action='secondServletpage';form.submit();">
					<input type="hidden" name="tweetid" id="tweetid" value=<%=tweetid%>>
					<input type="hidden" name="userid" id="userid" value=<%=userid%>></td>
			</form>
		</tr>
		<%
			}
		%>
	</table>
</body>
</html>