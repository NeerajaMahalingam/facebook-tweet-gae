package package_project_1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.apphosting.utils.config.ClientDeployYamlMaker.Request;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

/**
 * Servlet implementation class MyServlet
 */
@WebServlet("/MyServlet")
public class secondServletpage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String String = null;
	String tweetid = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public secondServletpage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String requestUrl = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/"));

		long tweetid = Long.parseLong(request.getParameter("tweetid").toString());
		String userid = request.getParameter("userid").toString();

		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		Key key = KeyFactory.createKey("Tweet", tweetid);
		System.out.println("Key value is:" + key);

		ds.delete(key);

		/* after servlet call land on tweetpage */
		response.sendRedirect(requestUrl + "/tweetpage.jsp?userid=" + userid);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
