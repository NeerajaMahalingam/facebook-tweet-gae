package package_project_1;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.apphosting.utils.config.ClientDeployYamlMaker.Request;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

/**
 * Servlet implementation class MyServlet
 */
@WebServlet("/MyServlet")
public class FirstServletpage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FirstServletpage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String page_userid = request.getParameter("user_id").toString();
		String userid, username, status, hLink;
		hLink = "";
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		// create entity without identifier
		Entity twt = new Entity("Tweet");
		twt.setProperty("user_name", request.getParameter("user_name"));
		twt.setProperty("user_id", request.getParameter("user_id"));
		System.out.println("user_dp" + request.getParameter("user_dp"));
		twt.setProperty("picture", request.getParameter("user_dp"));
		twt.setProperty("status", request.getParameter("content"));
		twt.setProperty("timestamp", new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
		twt.setProperty("visitedcnt", 0);
		ds.put(twt);

		// String requestUrl = request.getRequestURL().substring(0,
		// request.getRequestURL().lastIndexOf("/"));

		String requestUrl = "https://apps.facebook.com/189117331635390";
		Key key = KeyFactory.createKey("Tweet", request.getParameter("user_id"));
		System.out.println("Key value is:" + key);

		Query q = new Query("Tweet").addFilter("user_id", FilterOperator.EQUAL, request.getParameter("user_id"))
				.addSort("timestamp", SortDirection.DESCENDING);
		PreparedQuery pq = ds.prepare(q);
		for (Entity u1 : pq.asIterable()) {

			userid = u1.getProperty("user_id").toString();
			username = u1.getProperty("user_name").toString();
			status = u1.getProperty("status").toString();
			long tweetid = u1.getKey().getId();

			System.out.println("UserId:" + userid + "\t" + "Username:" + username + "\t" + "status::" + "\t" + status
					+ "tweetid::" + "\t" + tweetid);

			hLink = requestUrl + "/tweetpage.jsp?userid=" + page_userid + "&tweetid=" + tweetid;
			// "http://1-dot-snappy-climber-184705.appspot.com/displaypage.jsp?id=5649391675244544";

			request.setAttribute("tweetid", tweetid);
			request.setAttribute("userid", userid);
			request.getRequestDispatcher("tweetpage.jsp").forward(request, response);

			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
